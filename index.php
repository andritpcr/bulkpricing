<?php


date_default_timezone_set('America/New_York');

$config = require 'config.php';

require dirname(__FILE__).'/vendor/autoload.php';

use App\Core\BulkPricing;
// use App\Core\Router;
// use App\Core\Request;
use App\Core\Helpers;

$h = new Helpers();
$h->cors();

$bulkpricing = new BulkPricing();
$bulkpricing->callSessionStart();

// Show code errors for debuging
// Change these variables on config.php to debug the code
error_reporting($config['errorreporting']);
ini_set("display_errors",$config['displayerrors']);

require $config['appdir'] . '/routes.php';