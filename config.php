<?php 

return array(

	"rootdir" => "bulkpricing",

    "appdir" => dirname(__FILE__) . "/app",

    "errorreporting" 	=> E_ALL, // levels = E_ALL or -1 for all errors, 0 turn off all error reporting

    "displayerrors" 	=> "On", // either On or Off

    "restrictbyip"		=> "Off",

    "allowediplist"		=> array("10.2", "10.22.2"),


);

// pdocloc is not the actual value
// value here entered for testing purposes
// pdocloc will come from GENRFP file when SOEWAB - DESLOC - GENRFG programs are called 
// under the customerInfo sub-procedure in CGTVAP program.