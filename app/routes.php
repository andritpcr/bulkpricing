<?php 

use App\Core\Router;
use Symfony\Component\HttpFoundation\Request;

$request = Request::createFromGlobals();

$app = new Router();

$method = $request->server->get('REQUEST_METHOD');

// All method GET requests defined below this line
if( $method == 'GET' ){
    $app->get('/', 'AuthController@handleLogin');
	$app->get('/login/{id}', 'AuthController@login');
	$app->get('/home', 'PriceSheetController@initiate'); 
}

// All method POST requests defined below this line
if( $method === 'POST' ){

	$app->post('/uploadpricesheet', 'PriceSheetController@uploadSheet');
	$app->post('/pollpricesheetstatus', 'PriceSheetController@pollSheetInfo');

}

// Handle method fired once after all the routes have been added
$app->handle($request);





