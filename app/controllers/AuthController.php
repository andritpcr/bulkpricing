<?php

namespace App\Controllers;

use App\Core\BulkPricing;
use App\Core\Helpers;

class AuthController {

    private $projecturl;

	public function __construct()
	{

        $this->config = require(dirname(dirname(dirname(__FILE__))) . "/config.php");
		$this->BulkPricing = new BulkPricing;
        $this->Helpers = new Helpers;
        $this->authenticated = isset($_SESSION['authenticated']) && $_SESSION['authenticated'] == true ? true : false;
        $this->projecturl = basename(trim($this->config["rootdir"], '/'));

	}

	public function handleLogin()
    {

        if( $this->authenticated ) {
    
        	$this->Helpers->redirect('home');
    
        } else {
	
	        $this->BulkPricing->checkLoggedIn("BULKPRICING", $this->projecturl . "/login");
	
	    }
    
    }

    public function login($id)
    {

        if( isset($id) && $id != null ){

            $_SESSION["authenticated"] = true;
            $_SESSION["auth_id"] = $id;

            /**
             * GET USER DATA
             * This section gets all the information of the user that logged in
             * and stores them in the global $_SESSION array.
             *
             * Data Collected
             * [loggedIn]
             * [salesman_number]
             * [department]
             * [department_code]
             * [email]
             * [first_name]
             * [last_name]
             * [tfa_temp_key]
             * [custom] important for custom parameters being passed based on project requirements
             *
             * Uncomment the line below to activate
             */
            $this->getUserData($id);
            


        } else {

            $_SESSION["authenticated"] = false;
            $_SESSION["auth_id"] = '';

        }

        if( $_SESSION["authenticated"] ) {

            $this->Helpers->redirect('main');

        } else {

            $this->Helpers->redirect();

        }

    }

    private function getUserData($id)
    {

        $data = $this->BulkPricing->pcrTokenExchange($id);
        $this->BulkPricing->pcrLogger("Login Data", $data);
        if ($data != false){
            foreach( $data as $key => $val){
                if( gettype($val) === "array" ){
                    foreach( $val as $ckey => $cval ){
                        $_SESSION[$ckey] = $cval;
                    }
                } else {
                    $_SESSION[$key] = $val;
                }
            }
        }

    }

}

?>