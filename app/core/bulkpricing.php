<?php 
namespace App\Core;

use BaseClass;
use App\Core\Helpers;

class BulkPricing extends BaseClass {
    
    private $config;
    
    public function __construct() {
        
        //parent::__construct("env","PROJECT_LOG_FLAG", "PROJECT_LOG_FILE", "TEST_DATA", "TEST_LIBRARY", "page_auth_root");
        $env = parent::getEnv();
        //this class specific
        $this->config = require dirname(dirname(dirname(__FILE__))) . "/config.php";
        $this->application = $this->config["rootdir"];
        // $this->maxlifetime = 1800;
        $this->maxlifetime = 60;
        $this->Helpers = new Helpers();

        
        if ($env==="dev"){
            if( ! defined('LOG_FLAG') ){
                define("LOG_FLAG", true);
            }
            $this->session_path = '/pcr/php/sessions/pcrcgidev'. $this->application;
        }else{
            if( ! defined('LOG_FLAG') ){
                define("LOG_FLAG", false);
            }
            $this->session_path = '/pcr/php/sessions/pcrcgipbi'. $this->application;
        }
        
        parent::__construct(LOG_FLAG, "bulkPricing");
        
    }   
    
    /***************************************************/
    public function callSessionStart()
    {
        // # start the session! // # session save path, maxlifetime, cookie path valid
        parent::pcr_session_start($this->session_path, $this->maxlifetime, $this->application);
    }

    /**
     * @param  $appname
     * @param  $path
     * @return redirect to login page
     */
    public function checkLoggedIn($appname, $path ){

        if(isset($_REQUEST['id'])){
            $result=$this->PCRTokenExchange($_REQUEST['id']);
            var_dump($result);
            if(isset($result->salesman_number) && $result->salesman_number > 0) {
                $_SESSION["user_info"]=$result;
                $_SESSION["authenticated"] = 'true';
                $_SESSION["REMOTE_ADDR"]=$_SERVER["REMOTE_ADDR"];
            }
        }

        if ($_SESSION["authenticated"]!='true'){   
            $PAGE_AUTH_ROOT=$this->get_page_auth_root();
            header("Location: " . $PAGE_AUTH_ROOT . "?app=" . $appname . "&return_url=" . urlencode("https://" . $_SERVER["HTTP_HOST"] . "/php/" . $path ."/" ). "&seo=y");
            exit;
        }
    }
    
}