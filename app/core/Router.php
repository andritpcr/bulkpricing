<?php

namespace App\Core;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Exception\MethodNotAllowedexception;

class Router implements HttpKernelInterface
{

	protected $routes;

	public function __construct()
	{

		$this->routes = new RouteCollection();
		$this->controllerResolver = new ControllerResolver();

	}

	public function handle(Request $request, $type = HttpKernelInterface::MASTER_REQUEST, $catch = true)
	{

		$context = new RequestContext();

		$context->fromRequest($request);

		$matcher = new UrlMatcher($this->routes, $context);

		try {

			$request->attributes->add($matcher->match($request->getPathInfo()));

			$controller = $this->controllerResolver->getController($request);

			$arguments = $this->controllerResolver->getArguments($request, $controller);

			$response = call_user_func_array($controller, $arguments);

		} catch (ResourceNotFoundException $e) {

			$response = new Response('Not found!', Response::HTTP_NOT_FOUND);

			$response->prepare($request);
			$response->send();

		} catch (MethodNotAllowedException $e) {

			$response = new Response('Not found!', Response::HTTP_NOT_FOUND);

			$response->prepare($request);
			$response->send();

		} catch (Exception $exception) {

		    $response = new Response('An error occurred', 500);

		    $response->prepare($request);
		    $response->send();
		}

	}

	public function get($path, $controller)
	{

		$controller = 'App\Controllers' . '\\' . str_replace('@', '::', $controller);

		$this->routes->add($path, new Route(
			
			$path,

			array('_controller' => $controller),

			array(),

			array(),

			'',

			array(),

			array('GET')

		));

	}

	public function post($path, $controller)
	{

		$controller = 'App\Controllers' . '\\' . str_replace('@', '::', $controller);

		$this->routes->add($path, new Route(
			
			$path,

			array('_controller' => $controller),

			array(),

			array(),

			'',

			array(),

			array('POST')

		));

	}

}

?>