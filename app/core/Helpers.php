<?php

namespace App\Core;

// Global Helper Functions

class Helpers {

    private $config;

    public function __construct()
    {

        $this->config = require(dirname(dirname(dirname(__FILE__))) . "\config.php");
        $this->authenticated = isset($_SESSION['authenticated']) && $_SESSION['authenticated'] == true ? true : false;

    }

    public function cors() 
    {
        // Allow from any origin
        // die($_SERVER['HTTP_ORIGIN']);
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
            // you want to allow, and if so:
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: false');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
        }

        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                // may also be using PUT, PATCH, HEAD etc
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

            exit(0);
        }

        // echo "You have CORS!";
    }

    public function view($path, $data = array(), $extract = true, $data2 = array(), $extract2 = true)
    {
        
        if( $extract ){
            extract($data);
        }
        
        if( $extract2 ){
            extract($data2);
        }
        
        require $this->config["appdir"] . "/views/{$path}.view.php";
        
    }

    public function redirectToLogin()
    {

        if( ! $this->authenticated ){
            $this->redirect();
        }

    }

    public function redirect($path = '')
    {   
        $path = $this->config["rootdir"] . "/" . $path;
        header("Location: {$path}");
        exit();
        
    }

    public function loadPartial($name)
    {
        require $this->config["appdir"] . "/views/partials/" . $name . ".php";
    }

    /***************************************************/
    // This function is used to create auto versioning
    // for js and css files that will be changed often
    public function auto_versioning($file)
    {
        if( file_exists($file) ){
            return $file . "?v=" . filemtime($file);
        } else {
            return $file;
        }
    }

    /**
     * Generate a random string of length specified
     * @param integer $length 
     * @return string returns a random 16 char string concatenated with a time + date stamp, demarcated respectively with a T or a D
     */
    public function randomString($length, $incTimeStamp= 'true') {
        $time = time();
        $date = date('Ymd');
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));
    
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        if($incTimeStamp == 'true'){
            $key .= 'T' . $time;
            $key .= 'D' . $date;
        }
        
    
        return $key;
    }

    // Restrict access of website by the allowed list of IPs on the config array
    public function restrict_by_ip()
    {

        if($this->config['restrictbyip'] == 'On'){

            $allowedip = false;

            foreach($this->config['allowediplist'] as $ip){

                /*echo 'REMOTE_ADDR: ' . $_SERVER['REMOTE_ADDR'] . ' - ALLOWED IP: ' . $ip . '<br/>';

                echo 'STRPOS RESULT: ' . strpos($_SERVER['REMOTE_ADDR'], $ip) . '<br/>';*/

                if(strpos($_SERVER['REMOTE_ADDR'], $ip) === 0){

                    $allowedip = true;

                    break;

                }

            }

            if( $allowedip === false ){

                header("HTTP/1.0 404 Not Found");
                die();

            }

        }

    }

}


?>