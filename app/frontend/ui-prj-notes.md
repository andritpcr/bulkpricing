# Notes for UI

* /utils/PollServer.js: attempt to incorpoarte RxJS or Observables for long poll
* rxjs 6 interface breaks from 5..need to research what new syntax is doing

### Nov 3 2018: working on long poll.
* ~~ pulled the ajax calls out of Loading.js and into the App.js parent ~~
* ~~ working on async/await implementation in Long poll ~~


### Nov 5 2018
* ~~ TODO: create container components for every component that deals with state as SomeComponentContainer.jsx and use renderProps to pass data to SomeComponent.jsx, whihc acts like and index file in the module folder structure we use (also includes styles and small components exclusive to the whole module) ~~
    * should we use context or renderProps here?: Concerns: (hierarchy, reuse, level of each subcomponent implementation)
* ~~ TODO: change CSS to SCSS/SASS ~~

### Nov 6 2018
* ~~ try LoadingJS as a Portal ~~
* create other portals for drill down instructions or tooltips for each interface
* ~~ create portal for tooltip in SheetUpload.jsx ~~
* ~~ TODO:SheetUpload.jsx -> drop file : extract this to parent/child combo so can use animate HOC for this section ~~

### Nov 7 2018
* ~~ add Storybook support ~~

### Nov 12 2018
* TODO: is the rpgcgi working?  it worked manually in tests, but when I call it from UI, we arent getting anything back
* expand Storybook usage
* expand Jest usage