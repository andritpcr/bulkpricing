import React, { Component, Suspense, lazy } from 'react';
import axios from 'axios';

import './App.css';
import {Layout} from './Layout';

// import {loadJsonLocal, loadJsonHttp} from '../utils/fetchItems';
// import {getJSONResObj, postCorsResObj} from '../utils/ajaxRoutes';
// import {getBase64} from '../utils/utils';
import LoadingSpinner from '../utils/LoadingSpinner';

const SheetUpload = lazy(() => import ('./sheetUpload/SheetUpload'));
const Loading = lazy(() => import ('../containers/Loading'));
const FileReview = lazy(() => import ('../containers/FileReview'));
const TransactionInfo = lazy(() => import ('./TransactionInfo'));
const Table = lazy(() => import ('./Table'));
const Download = lazy(() => import ('./Download'));
const SubmitSheet = lazy(() => import ('./SubmitSheet'));

const SetShedule = lazy(() => import ('./setSchedule'));




const zonesfoundarray = [
  {name: 'zoneout', numofmodels: 7},
  {name: 'internet', numofmodels: 7},
  {name: 'cellphone', numofmodels: 7},
  {name: 'yoda', numofmodels: 7},
  {name: 'redux', numofmodels: 7},
  {name: 'react', numofmodels: 7},
];
const mfgfoundarray = [
  {name: 'scrolling', numofmodels: 8},
  {name: 'egghead', numofmodels: 8},
  {name: 'portals', numofmodels: 8},
  {name: 'avatar', numofmodels: 8},
  {name: 'css ninja', numofmodels: 8},
  {name: 'brightidea', numofmodels: 8},
  {name: 'spa chemistry', numofmodels: 8},
];

class App extends Component {
  //add to local storage for persistence
  state = {
        salesmanName: 'Mark Cuban',
        salesmanNumber: 0,
        ignoreinvalid: '0',
        skipoqf: '0',
        fileUploaded: false,
        fileUploading: localStorage.getItem("fileUploading") || false,
        uploadedFile: null,
        uploadedFileContents: [],
        rowsfound: 20,
        uniquemodelsfound: 10,
        zonesfound: zonesfoundarray.length,
        zonesarray: zonesfoundarray || [], 
        manufacturersfound: mfgfoundarray.length, 
        manufacturersarray: mfgfoundarray || [],
        uploadErr: false,
        errorInSheetAfterPrcs: false,
        sheetProcessStatus: 'none',
        uploadCheckBoxes: [],   
    };

    componentDidMount() {
      console.log('check is component', App.prototype.isReactComponent);
      console.log('check proto', Object.getOwnPropertyDescriptor(App, 'prototype'));
    }
    

    handleRemoveAllCheckboxes = () => {
      this.setState({ uploadCheckBoxes: [] })
    }
 
    //break this from longpoll, its just the file upload.  
  handleLongPoll = () => {
    const fileUpload = axios.post(process.env.REACT_APP_UPLOAD_URL, {
      file: this.uploadedFile,      
      ignoreinvalid: this.ignoreinvalid,
      skipoqf: this.skipoqf,
    },
    {
        headers: {
            // 'Content-Type': 'application/x-www-form-urlencoded'
            'Content-Type': 'multipart/form-data'
        }
    })
    .then(res=>{
        //from CGPURQ
        console.log('resdata id: ', res.data.id);
        //get id from res.data.id
        //  this.updateSalesmanNumber(res.data.salesman);
        this.updateSalesmanNumber(90595);
        return res.data.id;
        
      })
      .catch(error => {
        console.log('axios error: ', error.response);
      });

      // fileUpload
      // .then(() => this.doLongPoll() )
      // .catch(error => {
      //     console.log('file upload then longpoll error: ', error.response);
      //   }
      // );
  }

  

  updateFile = (file) => {
    return new Promise((resolve, reject) => {
      try{
       // console.log('in updateFile');
        this.setState({
          uploadedFile: file,
        })
        
        }
        catch(e){
          reject(e);
        }
    }); 
  }

  removeErr = () => {
    this.setState({
      uploadErr: false
    })
  }

  updateSalesmanNumber = (smn) => {
    this.setState({ salesmanNumber: smn})
  }


  longPollComplete = () => {
    console.log('in longPollComplete');
    return;
  }

  returnToUploadView = () => {
    console.log('in returnToUploadView');
    return;
  }

  handleUploadCheckboxes = (boxName) => {
    let ucb = this.state.uploadCheckBoxes;
    ucb.includes(boxName)
      ? this.setState({ uploadCheckBoxes: ucb.filter(e => {
            return e != boxName
          }) 
        })
      : this.setState({ uploadCheckBoxes: ucb.concat(boxName) })
  }

  

  //simple es5 way
//   handleLongPoll = () => {
//     //if status == null
//     let salesmannumber = this.state.salesmanNumber; 
//     console.log('in Ajax handleLongPoll JS: ', salesmannumber);  
//   //...runPoll  now sendPoll and handleLongPoll essentially doLongPoll
//     const interval = setInterval( () => {this.runPoll(salesmannumber)}, 5000 );
//     interval();
// }





  handleFileSend = (file) => {
    if(file){
        this.updateFile(file)
      .then(
        localStorage.setItem("uploadedFile", file)
      )
      .then(
        localStorage.setItem("fileUploading", "true")
      )
      .then(
        this.setState({
          fileUploading: true,  
        })
          
      )
    } else {
      this.setState({
        uploadErr: true
      })
         return;
    }
    
    };

    switchViewOnStatus = () => {
      switch(this.state.sheetProcessStatus){
        case 'Q':
          return (<Loading 
          returnToUploadView={this.returnToUploadView}
          longPollComplete={this.longPollComplete}
          loadingStatus={this.state.loadingStatus}
          ignoreinvalid={this.state.ignoreinvalid} 
          skipoqf={this.state.skipoqf}
          uploadedFile={this.state.uploadedFile}
          updateSalesmanNumber={this.updateSalesmanNumber}
          salesmanNumber={this.state.salesmanNumber}
          handleLongPoll={this.handleLongPoll} /> )
        break;

        case 'P':
          return (<Loading 
          returnToUploadView={this.returnToUploadView}
          longPollComplete={this.longPollComplete}
          loadingStatus={this.state.loadingStatus}
          ignoreinvalid={this.state.ignoreinvalid} 
          skipoqf={this.state.skipoqf}
          uploadedFile={this.state.uploadedFile}
          updateSalesmanNumber={this.updateSalesmanNumber}
          salesmanNumber={this.state.salesmanNumber}
          handleLongPoll={this.handleLongPoll} />) 
        break;

        case 'C':
         return( 
         <FileReview>
            <TransactionInfo 
              rowsfound={this.state.rowsfound} 
              uniquemodelsfound={this.state.uniquemodelsfound} 
              zonesfound={this.state.zonesfound} 
              zonesarray={this.state.zonesarray} 
              manufacturersfound={this.state.manufacturersfound} 
              manufacturersarray={this.state.manufacturersarray} />
            
            <Table uploadedFileContents={this.state.uploadedFileContents} />
            <div className="row">
              {this.state.errorInSheetAfterPrcs 
                ? <Download />
                : <React.Fragment><SetShedule/><SubmitSheet/></React.Fragment>
              }  
            </div>
          </FileReview>
          )
        break;

        case 'E':
          return ( 
          <FileReview>
            <TransactionInfo 
              rowsfound={this.state.rowsfound} 
              uniquemodelsfound={this.state.uniquemodelsfound} 
              zonesfound={this.state.zonesfound} 
              zonesarray={this.state.zonesarray} 
              manufacturersfound={this.state.manufacturersfound} 
              manufacturersarray={this.state.manufacturersarray} />
            
            <Table uploadedFileContents={this.state.uploadedFileContents} />
            <div className="row">
              {this.state.errorInSheetAfterPrcs 
                ? <Download />
                : <React.Fragment><SetShedule/><SubmitSheet/></React.Fragment>
              }  
            </div>
          </FileReview>
          )
        break;

        case 'S':
          return (<SheetUpload 
              handleRemoveAllCheckboxes={this.handleRemoveAllCheckboxes}
              handleUploadCheckboxes={this.handleUploadCheckboxes}
              handleFileSend={this.handleFileSend} 
              ignoreinvalid={this.state.ignoreinvalid} 
              skipoqf={this.state.skipoqf}
              uploadErr={this.state.uploadErr}
              removeErr={this.removeErr}  />)
        break;

        case 'X':
          return (<SheetUpload 
              handleRemoveAllCheckboxes={this.handleRemoveAllCheckboxes}
              handleUploadCheckboxes={this.handleUploadCheckboxes}
              handleFileSend={this.handleFileSend} 
              ignoreinvalid={this.state.ignoreinvalid} 
              skipoqf={this.state.skipoqf}
              uploadErr={this.state.uploadErr}
              removeErr={this.removeErr}  />)
        break;

        default: 
          return (<SheetUpload 
              handleRemoveAllCheckboxes={this.handleRemoveAllCheckboxes}
              handleUploadCheckboxes={this.handleUploadCheckboxes}
              handleFileSend={this.handleFileSend} 
              ignoreinvalid={this.state.ignoreinvalid} 
              skipoqf={this.state.skipoqf}
              uploadErr={this.state.uploadErr}
              removeErr={this.removeErr}  />)
        break;
      }
    }

  render() {
    const {sheetProcessStatus, errorInSheetAfterPrcs} = this.state;
    // const fileReview = 
    //   <FileReview>
    //     <TransactionInfo 
    //       rowsfound={this.state.rowsfound} 
    //       uniquemodelsfound={this.state.uniquemodelsfound} 
    //       zonesfound={this.state.zonesfound} 
    //       zonesarray={this.state.zonesarray} 
    //       manufacturersfound={this.state.manufacturersfound} 
    //       manufacturersarray={this.state.manufacturersarray} />
        
    //     <Table uploadedFileContents={this.state.uploadedFileContents} />
    //     <div className="row">
    //       {errorInSheetAfterPrcs 
    //         ? <Download />
    //         : <React.Fragment><SetShedule/><SubmitSheet/></React.Fragment>
    //       }  
    //     </div>
    // </FileReview>;

    //TODO: use sheetProcessStatus state to determine which view to show and determine if need fileUploaded/fileUploading checks?  

    return (
      <Layout buyerName={this.state.salesmanName}>
        <Suspense fallback={<LoadingSpinner/>}>
          {sheetProcessStatus === 'none'
            ? this.state.fileUploaded 
              ? <FileReview>
                  <TransactionInfo 
                    rowsfound={this.state.rowsfound} 
                    uniquemodelsfound={this.state.uniquemodelsfound} 
                    zonesfound={this.state.zonesfound} 
                    zonesarray={this.state.zonesarray} 
                    manufacturersfound={this.state.manufacturersfound} 
                    manufacturersarray={this.state.manufacturersarray} />
                  
                  <Table uploadedFileContents={this.state.uploadedFileContents} />
                  <div className="row">
                    {errorInSheetAfterPrcs 
                      ? <Download />
                      : <React.Fragment><SetShedule/><SubmitSheet/></React.Fragment>
                    }  
                  </div>
                </FileReview>
              : this.state.fileUploading 
                ? <Loading 
                    loadingStatus={this.state.loadingStatus}
                    ignoreinvalid={this.state.ignoreinvalid} 
                    skipoqf={this.state.skipoqf}
                    uploadedFile={this.state.uploadedFile}
                    updateSalesmanNumber={this.updateSalesmanNumber}
                    handleLongPoll={this.handleLongPoll} /> 
                : <SheetUpload 
                    handleRemoveAllCheckboxes={this.handleRemoveAllCheckboxes}
                    handleUploadCheckboxes={this.handleUploadCheckboxes}
                    handleFileSend={this.handleFileSend} 
                    ignoreinvalid={this.state.ignoreinvalid} 
                    skipoqf={this.state.skipoqf}
                    uploadErr={this.state.uploadErr}
                    removeErr={this.removeErr}  />
            :   this.switchViewOnStatus 
          }    
        </Suspense>  
      </Layout>
    );
  }
}

export default App;
