import React, {Component} from 'react';

import './submitsheet.scss';

class SubmitSheet extends Component {

    handleSubmitBtnClick = (e) => {
        e.preventDefault();
        console.log('dl clicked: ', e);
    }
    render() {
        const {priceSheetTitle} = this.props;
        return(
            <section className="col-sm-4 submit-sheet-section">
                <header><h2>Submit Price Sheet</h2></header>
                <div className="submit-sheet-inner col-xs-6">
                    <button className="Submit-btn" onClick={this.handleSubmitBtnClick}>
                        <span className="go-left">
                            <i className="fas fa-Submit"></i>
                        </span> 
                        <span className="go-right">Submit</span>
                    </button>
                    <p>{priceSheetTitle ? priceSheetTitle : 'Name of Price Sheet'}</p>
                </div>
                <div className="col-xs-6">
                    <h3>✅ SUCCESS!</h3>
                </div>

            </section>)
    }
}

export default SubmitSheet;