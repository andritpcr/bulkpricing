import React from 'react';
import ReactDOM from 'react-dom';
import {ToolTipAnimateHOC} from './ToolTipAnimateHOC';
let el = document.createElement('div');
const modalRoot = document.getElementById('modal_root');
class ToolTipModal extends React.Component {
  
  componentDidMount() {
    modalRoot.appendChild(el);
  }
  componentWillUnmount() {
    modalRoot.removeChild(el);
  }

  render() {
    return ReactDOM.createPortal(
      <div
        style={{
          position: 'absolute',
          top: '0',
          bottom: '0',
          left: '0',
          right: '0',
          display: 'grid',
          justifyContent: 'center',
          alignItems: 'center',
          transition: 'all .5s ease-in-out',
          backgroundColor: 'rgba(0,0,0,0.3)',
        }}
        onClick={this.props.onClose}
      >
        <div
          style={{
            padding: 20,
            background: '#fff',
            borderRadius: '2px',
            display: 'inline-block',
            minHeight: '300px',
            margin: '1rem',
            position: 'relative',
            minWidth: '300px',
            transition: 'all .5s ease-in-out',
            boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
            justifySelf: 'center',
          }}
        >
          {this.props.children}
          <hr />
          <button onClick={this.props.onClose}>Close</button>
        </div>
      </div>,
      el,
    )
  }
}

export default ToolTipAnimateHOC(ToolTipModal);
