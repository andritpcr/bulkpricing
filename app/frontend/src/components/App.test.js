import React from 'react';
import ReactDOM from 'react-dom';
// import {getQueriesForElement} from 'dom-testing-library';
// import {render} from 'react-testing-library';
import {shallow} from 'enzyme';
import App from './App';

// function render(ui){
//   const container = document.createElement('div');
//   ReactDOM.render(ui, container);
//   const queries = getQueriesForElement(container);
//   return {
//     container,
//     ...queries,
//   }
// }


describe('<App/>', () => {
  
  describe('sample test', () => {
    it('should render app', () => {
      const wrapper = shallow(<App/>);
      console.log(wrapper.debug());
    })
  })


  describe('Testing that salesman name is populated', () => {
    it('have the correct salesman name', () => {
      const wrapper = shallow(<App/>);
      expect(wrapper.state('salesmanName')).toBe('Mark Cuban');
    })
  })

})