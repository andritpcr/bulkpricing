import React, {Component} from 'react';
import {TransitionAnimationHOC} from '../../utils/TransitAnimeHOC';

class UploadListing extends Component{
    state = {
        checkBoxOne: false,
        checkBoxTwo: false,
    }
    handleUpdateCheckbox = (e) => {
        let boxName = e.target.name;
        boxName === "ignoreBadCheckBox" 
            ? this.setState(({checkBoxOne}) => ({
                checkBoxOne: !checkBoxOne
            }))
            : this.setState(({checkBoxTwo}) => ({
                checkBoxTwo: !checkBoxTwo
            }));
        this.props.updateListingCheckbox(boxName);
    }
    render(){
        const {fileData, removeFileItem} = this.props;
        return (
            <React.Fragment>
                { 
            fileData.map(f => {
                    return(
                     <div key={f.name} className="col-xs-12">
                         <div className="col-xs-11">
                            <p><strong>{f.name}</strong> - {f.size} bytes - {f.type} type</p>
                            <label htmlFor="ignore-bad" className="col-xs-6">Ignore Invalid Models: <input 
                                                                                                        type="checkbox" 
                                                                                                        onChange={this.handleUpdateCheckbox}
                                                                                                        checked={this.state.checkBoxOne} 
                                                                                                        name="ignoreBadCheckBox" />
                            </label>
                            <label htmlFor="skip-oqf" className="col-xs-6">Skip Oppy/Quit/Frozen: <input 
                                                                                                    type="checkbox" 
                                                                                                    onChange={this.handleUpdateCheckbox} 
                                                                                                    checked={this.state.checkBoxTwo} 
                                                                                                    name="skipOqfCheckBox" />
                            </label>
                         </div>
                         <div onClick={removeFileItem} className="col-xs-1 remove-list-item center-text">
                            <p className="fas fa-times"><br/>Remove</p>
                            <p></p>
                         </div>
                     </div>
                    )    
             })
             }
            </React.Fragment>
        )
    }
}

const UploadListingAnime = TransitionAnimationHOC(UploadListing);
export default UploadListingAnime;