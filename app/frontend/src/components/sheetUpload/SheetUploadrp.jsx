import React, {PureComponent} from 'react';
import './sheetupload.scss';
import Dropzone from 'react-dropzone';
import UploadListingAnime from './UploadListing';
import SheetUploadContainer from './SheetUploadContainer';


const SheetUpload = ({onDrop, uploadingFiles, removeListItem, handleFileSend, uploadErr}) => {
    
        return(
            <SheetUploadContainer children={props => (
                <section className="row upload-form-section">
                    <header className="row">
                        <div className="col-sm-12 top-header">
                        <h1>Price Sheet Processing</h1>
                        
                        <a className="download-template" 
                            href="/assets/files/BulkPricingTemplate_V1.xlsx" 
                            download="Price Sheet Template">
                            Pricing Template Download 
                                    <i className="fas fa-download"></i>
                                </a>
                            
                        </div>
                        <div className="col-md-6 border-right">
                            <h3>File Upload</h3>
                        </div>
                        <div className="col-md-6">
                            <h3>Included Files</h3>
                        </div>
                    </header>
                
                    <div className="row">
                        <div className="dropzone-wrap col-md-5 col-md-offset-1">
                            <Dropzone 
                                accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                                className="dropzone-inner" 
                                name="file-dropzone"
                                onDrop={props.onDrop}>
                                <h4>Upload Files</h4>
                                <p><i className="fas fa-cloud-upload-alt"></i></p>
                                <h6>DRAG AND DROP</h6>
                                <h4>OR</h4>
                                <div className="upload-browse">BROWSE FILES</div>
                            </Dropzone>
                        </div>
                        <aside className="col-md-5">
                            <UploadListingAnime fileData={props.uploadingFiles} removeFileItem={props.removeListItem} />
                            
                        </aside>
                        <p className={props.uploadErr ? 'show-error' : 'hide-error'}>Please upload a file to review before continuing.</p>
                    </div>
                    <div className="btn-wrap row">
                    
                        <button 
                            type="button" 
                            className="btn" 
                            onClick={props.handleFileSend}>Check your File For errors</button>
                    </div>
                </section>
                )}>
            </SheetUploadContainer>
            
            
        )

}

export default SheetUpload;
