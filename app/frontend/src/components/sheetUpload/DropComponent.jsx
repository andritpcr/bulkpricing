import React, { Component} from 'react';
import Dropzone from 'react-dropzone';
import {TransitionAnimationHOC} from '../../utils/TransitAnimeHOC';

class DropComponent extends Component {
    state={
        fileToUpload: null
    }
    onDrop = (files) => {
        this.fileRead(files)
        .then(
         this.props.handleDrop(files)
        )
        .catch(error => {
         console.log('file error: ', error.response);
       });
    
   }

   fileRead = (files) => {
    return new Promise((res, rej) => {
        try{
             const reader = new FileReader();
             if(files){
                 reader.readAsDataURL(files[0]);
             }
 
             reader.onloadend = () =>{
                 this.setState({
                    fileToUpload: reader.result 
                 })
             }
             reader.onabort = () => console.log('file reading was aborted');
             reader.onerror = () => console.log('file reading has failed');
        }
        catch(e){
            rej(e);
        }
     });
 }
    render(){
        return(
            <Dropzone 
                accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                className="dropzone-inner" 
                name="file-dropzone"
                onDrop={this.onDrop}>
                
                <div className="title-section">
                    <h4>Upload Files</h4>    
                </div>
                
                <p><i className="fas fa-cloud-upload-alt"></i></p>
                <h6>DRAG AND DROP</h6>
                <h4>OR</h4>
                { this.state.fileToUpload !== null ? <div className="fa fa-check-circle green-font"></div> : <div className="upload-browse">BROWSE FILES</div> }
            </Dropzone>
        )

    }
}

export default TransitionAnimationHOC(DropComponent);