import React, {PureComponent, Suspense, lazy } from 'react';
import './sheetupload.scss';

// import UploadListingAnime from './UploadListing';
// import DropComponent from './DropComponent';
import ToolTipUpload from '../ToolTipModal';

const UploadListingAnime = lazy(() => import ('./UploadListing'));
const DropComponent = lazy(() => import ('./DropComponent'));

class SheetUpload extends PureComponent{
    state={
            uploadingFiles: [],
            fileToUpload: null, 
            showUploadModal: false,   
        }

    handleShowMessageClick = (e) => {
        e.preventDefault();
        this.setState({showUploadModal: true});
    };
    handleCloseToolTipUpload = () => this.setState({showUploadModal: false});

    handleDrop = (files) => {
        this.props.removeErr();
        this.setState({
            uploadingFiles: files
        });
    }

    updateListingCheckbox = (boxName) => {
        this.props.handleUploadCheckboxes(boxName);
    }

    removeListItem = () =>{
      this.setState({
          uploadingFiles: [],
          fileToUpload: null   
      }, this.props.handleRemoveAllCheckboxes())
    }

    //   handleFileSend = (e) => {
    //     e.preventDefault();
    //    // console.log('sending file: ', this.state.fileToUpload); 
    //     this.updateFile(this.state.fileToUpload)
    //       .then(() => {
    //           this.props.handleFileSend(this.state.fileToUpload);
    //       })
    // }

      handleFileSend = (e) => {
          e.preventDefault();
         // console.log('sending file: ', this.state.fileToUpload); 
          this.props.handleFileSend(this.state.uploadingFiles);
      }
    render(){
        return(
            <section className="row upload-form-section">
                <header className="row">
                    <div className="col-sm-12 top-header">
                        <div className="tooltip-container">
                            <i className="fa fa-question-circle" 
                                onClick={this.handleShowMessageClick}></i>
                        </div>
                        <h1 >Price Sheet Processing</h1> 
                    </div>
                    <div className="col-md-12 section-title">
                        {this.state.uploadingFiles.length > 0 ? <h3>Included Files</h3> : <h3>File Upload</h3> }
                    </div>
                </header>
               
                <div className="row">
                    {this.state.showUploadModal ? (
                            <ToolTipUpload className="tooltip-modal" onClose={this.handleCloseToolTipUpload}>
                                <header>Welcome to the PC Richard &amp; Son Bulk Price Sheet Processor!</header>
                                <p>You can upload one .xlsx filetype price sheet at a time.</p>
                                <ol>
                                    <li>Start by uploading your sheet by dragging from a Finder window to the Upload Box or click 'Browse Files'</li>
                                    <li>The screen will render information about your upload and you can verify that the correct file has been staged in the browser and ready to send to the server.
                                        <ul>
                                            <li>You can also check a few options for special cases</li>
                                        </ul>
                                    </li>
                                    <li>Once you're ready to send it to the server to be analyzed, click the "Check your File For Errors" button and wait for your results</li>

                                </ol>
                            </ToolTipUpload>
                        ) : null}

                    {this.state.uploadingFiles.length > 0
                        ?   <React.Fragment>
                                <aside className="col-md-12">
                                    <Suspense fallback={<div>Loading File...Please wait to Validate...</div>}>
                                        <UploadListingAnime fileData={this.state.uploadingFiles}
                                                            updateListingCheckbox={this.updateListingCheckbox} 
                                                        removeFileItem={this.removeListItem} /> 
                                    </Suspense>   
                                </aside>
                                <p className={this.props.uploadErr ? 'show-error' : 'hide-error'}>Please upload a file to review before continuing.</p>
                            </React.Fragment>
                        :   <div className="dropzone-wrap col-md-12">
                                <Suspense fallback={<div>Drag and Drop loading, please wait...</div>}>
                                    <DropComponent handleDrop={this.handleDrop} />
                                </Suspense>     
                            </div>
                    }
                </div>
                <div className="btn-wrap row">
                
                    {this.state.uploadingFiles.length > 0
                        ? <button type="button" 
                                    className="btn" 
                                    onClick={this.handleFileSend}>
                                        Check your File For errors
                            </button>
                        : <a className="download-template" 
                                href="/assets/files/BulkPricingTemplate_V1.xlsx" 
                                download="Price Sheet Template">
                                    Pricing Template Download 
                                    <i className="fas fa-download"></i>
                            </a>
                        // <button type="button" 
                        //         className="btn download-template" 
                        //         onClick="window.open('/assets/files/BulkPricingTemplate_V1.xlsx')"
                        //         download="Price Sheet Template">
                        //             Pricing Template Download 
                        //         <i className="fas fa-download"></i>
                        // </button>
                    }
                </div>

            </section>
            
        )
    }
}

export default SheetUpload;