import React, {PureComponent} from 'react';


class SheetUploadContainer extends PureComponent{
    state={
            uploadingFiles: [],
            FileToUpload: null, 
            uploadError: this.props.uploadErr,   
        }

    fileRead = (files) => {
        return new Promise((res, rej) => {
            try{
                 const reader = new FileReader();
                 if(files){
                     reader.readAsDataURL(files[0]);
                 }
     
                 reader.onloadend = () =>{
                     this.setState({
                         FileToUpload: reader.result 
                     })
                 }
                 reader.onabort = () => console.log('file reading was aborted');
                 reader.onerror = () => console.log('file reading has failed');
            }
            catch(e){
                rej(e);
            }
         });
     }

    onDrop = (files) => {
           this.fileRead(files)
           .then(
            this.props.removeErr()
           ).then(
               this.setState({
                    uploadingFiles : files   
                })

           )
           .catch(error => {
            console.log('file error: ', error.response);
          });
       
      }
      removeListItem = () =>{
        this.setState({
            uploadingFiles : [],
            FileToUpload: null   
        })
      }

      handleFileSend = (e) => {
          e.preventDefault();
         // console.log('sending file: ', this.state.FileToUpload);
         
          this.props.handleFileSend(this.state.FileToUpload);
      }
    render(){
        return this.props.children(this.state.uploadingFiles, 
                                    this.onDrop, 
                                    this.removeListItem,
                                    this.handleFileSend,
                                    this.state.uploadErr);
    }
}

export default SheetUploadContainer;