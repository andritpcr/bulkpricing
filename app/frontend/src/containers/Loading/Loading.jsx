import React, {Component} from 'react';
import anime from 'animejs';
import axios from 'axios';


import './loading.scss';


class Loading extends Component{
    //depending what we get from RPG, maybe this is a prop...
    state = {
        loadingStatus: 'Please wait while we organize the data...',
    }
    componentDidMount() {
        anime({
            targets: '.box',
            translateX: '53.5rem',
            scale: [.75, .9],
            delay: function(el, index) {
              return index * 360;
            },
            direction: 'alternate',
            loop: true,
            easing: 'easeInOutQuart'
          });
          anime({
            targets: '.box-right',
            translateX: '53.5rem',
            scale: [.75, .9],
            delay: function(el, index) {
              return index * 270;
            },
            direction: 'alternate',
            loop: true,
            easing: 'easeInOutQuart',
            elasticity: function(el, i, l) {
                return (200 + i * 200);
              }
          });
        //   this.props.handleLongPoll(); 
        this.doLongPoll();
       
    }

    doLongPoll = () => {
   
        let pollUrl = process.env.REACT_APP_POLL_URL;
        let salesmannumber = this.props.salesmanNumber, statusState; 
        
        // const initPoll = await this.sendPoll(salesmannumber, pollUrl).then(res => this.checkHeadersForStatus(res.data.status));
        const initPoll = this.sendPoll(salesmannumber, pollUrl)
                                    .then(res => {
                                      statusState = res.data.status;
                                      return statusState;
                                    });
        initPoll
          .then(this.checkHeadersForStatus(statusState));
        //const checkHeaders = await this.checkHeadersForStatus(data);
        //const responseObject = await initPoll.then(res => res.data.status);
        console.log('doLongPoll data', statusState);
        console.log('doLongPoll url', pollUrl);
  
    }
  
    sendPoll = (salesman, ajaxurl) => {
      console.log('in sendPoll', ajaxurl);
      return axios.post(ajaxurl, {
        "salesmannumber": salesman
      },
      {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
            // 'Content-Type': 'multipart/form-data'
        }
    })
      // .then( res => {
      //     // let status = res.status ?  res.status : 'none';  
      //     console.log('in runPoll JS: ', salesman);
      //     console.log('in runPoll JS res : ', res);
      //     //causes loading screen to change becasue of ternary on render->return
      //     // let status = res.data.status;  
      //     // this.setState({
      //     //   sheetProcessStatus: status
      //     // })
      //   }
      // )
    //   .catch(error => {
    //     console.log('polling error: ', error.response);
    //   }
    // )
    }
  
    checkHeadersForStatus = (status) => {
      //check header status everytime app is open
      switch(status){
          case 'Q':
          //processing view
          //return value for component to app componet to set 
            this.setState({ loadingStatus: status}, setTimeout(() => this.doLongPoll(), 500));
            console.log(status);
          break;
          case 'P':
          //processing view
            console.log(status);
            this.setState({ loadingStatus: status}, setTimeout(() => this.doLongPoll(), 500));
          break;
          case 'C':
          //Review view
            this.props.longPollComplete();
            console.log(status);
          break;
          case 'E':
          //Review view
            this.props.longPollComplete();
            console.log(status);
          break;
          case 'S':
          //Upload view
          this.props.returnToUploadView();
            console.log(status);
          break;
          case 'X':
          //Upload view
          this.props.returnToUploadView();
            console.log(status);
          break;
          case 'norel':
          //upload view
            this.props.returnToUploadView();
            console.log(status);
          break;
          default:
            //upload view
            this.returnToUploadView();
            console.log('default ', status);
          break;
      }
    }
  

    componentDidUpdate(prevProps, prevState) {
        if(this.state.loadingStatus !== prevState.loadingStatus){
            //if loadingStatus is done, go to next view
            //else update the screen to show new message 
            // .then(runNextPoll)  
                //add setTimeout to buffer the calls
        }
    }
    
    
    render(){
        let {loadingStatus} = this.state;
        return(
            <React.Fragment>
               <h2 className="loading-boxes-header">{loadingStatus}</h2>
                <div className="boxes-wrapper">
                    <div className="box"></div>
                    <div className="box"></div>
                    <div className="box"></div>
                </div>
                <div className="boxes-wrapper-right">
                    <div className="box-right"></div>
                    <div className="box-right"></div>
                    <div className="box-right"></div>
                </div>
            </React.Fragment>
           

        )
    }
}

export default Loading;