import React, {Component} from 'react';

//make this a HOC provider

class FileReview extends Component{
    render() {
        return(
            <div className="file-review-wrap">
                {this.props.children}
                {/* {this.props.children(this.state)} */}
            </div>
        )
    }
}

export default FileReview;