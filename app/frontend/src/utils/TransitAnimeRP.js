import React, {Component} from 'react';
import Transition from 'react-transition-group/Transition';

const duration = 400;
const duration2 = 800;

const defaultStyle = {
  transition: `opacity ${duration2}ms ease-in-out, right ${duration}ms ease-in-out`,
  opacity: 0,
  top: 0,
  right: 1000,
  position: 'relative'
}

const transitionStyles = {
  entering: { opacity: 0, right: 1000 },
  entered:  { opacity: 1, right: 0 },
};

class TransitionAnimationRP extends Component {

        state = {in:false}

        componentDidMount() {
            this.setState({ in: true })
        }
        // componentWillUnmount(){
        //     clearTimeout();
        // }
        
        render(){
            return(
                <Transition 
                    in={this.state.in} 
                    timeout={{
                        enter: duration,
                        exit: duration,
                       }}
                    unmountOnExit >
                    {(state) => {
                            { this.props.children({
                                ...defaultStyle,
                                ...transitionStyles[state]
                            }) }
                        }
                    }
                </Transition>
            )
        }
}

export default TransitionAnimationRP;