import { Observable, from, timer } from 'rxjs';
import { map, concatMap } from 'rxjs/operators';

const POLLING_INTERVAL = 600; // in milliseconds

// export const pollServer = (fetchFn, isSuccessFn, pollInterval = POLLING_INTERVAL) => {
//   return interval(pollInterval).pipe(
//       switchMap(() => from(fetchFn())),
//       takeWhile((response) => isSuccessFn(response))
//   );
// }

// export const pollObserver = (ajaxFn) => {
//     Rx.Observable.create((o) => {
//         ajaxFn
//             .then((data) => {
//                 o.onNext(data);
//                 o.onCompleted();
//             })
//             .fail(o.onError)
//     })
// } 

//work on this to make it work for this instance
export const pollObserver = merge(
    of(true),
    timer(0, 500)
      .pipe(concatMap(() => from(fetch(`/tasks/${taskId}`))
        .pipe(map(response => response.json())))
      )
      .pipe(filter(backendData => backendData.processing === false))
  )
      .pipe(take(2))
      .pipe(map(processing => processing ? '⏳' : '✅'));